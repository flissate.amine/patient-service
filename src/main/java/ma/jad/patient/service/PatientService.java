package ma.jad.patient.service;

import ma.jad.patient.entities.Patient;
import ma.jad.patient.repository.PatientRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

@Service
public class PatientService {
    private static final Logger LOGGER = LoggerFactory.getLogger(PatientService.class);
    private final PatientRepository patientRepository;

    public PatientService(PatientRepository patientRepository) {
        this.patientRepository = patientRepository;
    }

    public void deletePatientById(Long idPatient) {
        if (idPatient == null) {
            LOGGER.warn("ID patient cannot be null or blank");
        }
        try {
            LOGGER.info("incoming request to delete patient with ID:{}", idPatient);
            if (idPatient != null) {
                patientRepository.deleteById(idPatient);
                LOGGER.info(" patient with ID:{} was deleted successfully", idPatient);
            }
        } catch (Exception e) {
            LOGGER.error("something goes wrong , cant delete this patient ");
        }
    }

    public void savePatient(Patient patient) {
        if (patient == null) {
            LOGGER.warn("can't save this patient / Patient = null, please check patient format");
        }
        try {
            patientRepository.save(patient);
        } catch (Exception e) {
            e.printStackTrace();
            LOGGER.error("something wrong , cant save this patient ");
        }
    }
}
