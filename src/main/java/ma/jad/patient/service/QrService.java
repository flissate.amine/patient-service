package ma.jad.patient.service;

import com.google.zxing.WriterException;
import org.springframework.stereotype.Service;

import java.io.IOException;

@Service
public interface QrService {

    void generateQrCode(String fileContent, int width, int height, String path) throws WriterException, IOException;
    byte[] getQrCodeImage(String text, int width, int height) throws WriterException, IOException;
}
