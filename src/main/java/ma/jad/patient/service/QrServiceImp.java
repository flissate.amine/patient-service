package ma.jad.patient.service;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.WriterException;
import com.google.zxing.client.j2se.MatrixToImageConfig;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Path;

@Service
public class QrServiceImp implements QrService {

    private Logger logger = LoggerFactory.getLogger(QrServiceImp.class);

    @Override
    public void generateQrCode(String text, int width, int height, String filePath) throws WriterException, IOException {
        QRCodeWriter qrwriter = new QRCodeWriter();
        BitMatrix matrix = qrwriter.encode(filePath, BarcodeFormat.CODABAR, width, height);
        Path fileP = FileSystems.getDefault().getPath(filePath);
        MatrixToImageWriter.writeToPath(matrix, "PNG", fileP);
    }

    @Override
    public byte[] getQrCodeImage(String text, int width, int height) throws WriterException, IOException {
        QRCodeWriter qrwriter = new QRCodeWriter();
        BitMatrix matrix = qrwriter.encode(text, BarcodeFormat.CODABAR, width, height);
        ByteArrayOutputStream pngOutputStream = new ByteArrayOutputStream();
        MatrixToImageConfig con = new MatrixToImageConfig();
        MatrixToImageWriter.writeToStream(matrix, "PNG", pngOutputStream, con);
        return pngOutputStream.toByteArray();
    }
}
