package ma.jad.patient.controller;

import com.google.zxing.WriterException;
import ma.jad.patient.service.QrService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;

@CrossOrigin("*")
@RestController("Qr")
public class QrController {
    @Autowired
    private QrService qrService;

    String path;

    @GetMapping(path = "/qr",produces = MediaType.IMAGE_JPEG_VALUE)
    @ResponseBody
    public byte[] getQrCode(@RequestParam String text, int width, int height) throws WriterException, IOException {

        byte[] qrCodeImage = qrService.getQrCodeImage(text, 250, 250);

        qrService.generateQrCode(text, width, height, path);
        return qrCodeImage;
    }

}
