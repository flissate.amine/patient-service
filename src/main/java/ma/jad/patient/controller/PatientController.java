package ma.jad.patient.controller;

import lombok.extern.slf4j.Slf4j;
import ma.jad.patient.entities.Patient;
import ma.jad.patient.repository.PatientRepository;
import ma.jad.patient.service.PatientService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.ws.rs.core.MediaType;
import java.util.List;

import static ma.jad.patient.config.Constant.SECURED_URL;

@Slf4j
@RestController
@CrossOrigin(origins = "*")
public class PatientController {

    private static final Logger LOGGER = LoggerFactory.getLogger(PatientController.class);

    private final PatientRepository patientRepository;
    private final PatientService patientService;

    public PatientController(PatientRepository patientRepository, PatientService patientService) {
        this.patientRepository = patientRepository;
        this.patientService = patientService;
    }

    @GetMapping(path = SECURED_URL + "/all",
            produces = MediaType.APPLICATION_JSON
    )
    @ResponseBody
    public ResponseEntity<Page<Patient>> getAllPatients(@RequestParam(name = "page", defaultValue = "0") int page,
                                                        @RequestParam(name = "size", defaultValue = "5") int size) {
        LOGGER.info("incoming request to get patients page");
        PageRequest pageRequest = PageRequest.of(page, size);
        Page<Patient> pageResult = patientRepository.findAll(pageRequest);
        return ResponseEntity.ok().body(pageResult);
    }

    @GetMapping(path = SECURED_URL + "/{idPatient}", produces = MediaType.APPLICATION_JSON)
    @ResponseBody
    public ResponseEntity<Patient> getPatientById(@PathVariable(name = "idPatient") Long idPatient)
            throws ResourceNotFoundException {
        LOGGER.info("incoming request to patient with ID :  {}", idPatient);
        Patient patient = patientRepository.findById(idPatient)
                .orElseThrow(() -> new ResourceNotFoundException("cannot fetch patient with id " + idPatient));
        return ResponseEntity.ok().body(patient);
    }

    @DeleteMapping(path = SECURED_URL + "/{idPatient}")
    public void deletePatientById(@PathVariable("idPatient") Long idPatient) {
        patientService.deletePatientById(idPatient);
    }

    @PostMapping(path = SECURED_URL + "/save", consumes = MediaType.APPLICATION_JSON)
    public void savePatient(@RequestBody Patient patient) {
        LOGGER.info("incoming request to save patient with name {} and id : {}", patient.getName(), patient.getIdPatient());
        patientService.savePatient(patient);
        LOGGER.info("patient with name {} was saved successfully", patient.getName());
    }

    @GetMapping(path = SECURED_URL + "/search", produces = MediaType.APPLICATION_JSON)
    @ResponseBody
    public ResponseEntity getPatientByNameContains(@RequestParam(name = "kw") String kw) {
        LOGGER.info("search patient with keyword contains :{} ", kw);
        if (kw != null) {
            List<Patient> result = patientRepository.findPatientByNameContains("%" + kw + "%");
            return ResponseEntity.ok().body(result);
        } else {
            LOGGER.warn("no kw received with request , please check");
            int page = 0;
            int size = 5;
            PageRequest pageRequest = PageRequest.of(page, size);
            Page<Patient> all = patientRepository.findAll(pageRequest);
            return ResponseEntity.ok().body(all);
        }
    }
}
