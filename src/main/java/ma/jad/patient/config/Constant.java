package ma.jad.patient.config;

public class Constant {

    public static final String FREE_ACCESS_API = "/api/**";
    public static final String SECURED_API = "/auth/**";
    public static final String LOGIN_PAGE = "http://localhost:4200/login";
    public static final String SECURED_URL = "/auth";
    public static final String ANONYMIZE_URL = "/api";
}
