package ma.jad.patient.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.*;
import java.util.Date;

@Data
@Entity 
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Table(name = "PATIENT")
public class Patient {
	@Id @GeneratedValue(strategy = GenerationType.AUTO)
	private Long idPatient;
	@Column(name = "P_NAME")
	private String name;
	@Column(name = "P_LNAME")
	private String prenom;
	@Column(name = "STATUS")
	private Boolean malade = true;
	@Column(name = "P_BIRTHDATE")
	private String dateNaissance;
	@Column(name = "P_ADRESS")
	private String adresse;
	@Column(name = "P_TEL")
	private long tel;
	private String detailsConsultations;
	private Boolean firstTime;
	private long idFacture;
	private Date dateConsultation;
	private String etat;
	private String observations;
	}
	

