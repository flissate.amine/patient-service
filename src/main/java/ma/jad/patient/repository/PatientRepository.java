package ma.jad.patient.repository;

import ma.jad.patient.entities.Patient;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.List;

@RepositoryRestResource
public interface PatientRepository extends PagingAndSortingRepository<Patient, Long> {
    @Query("SELECT p FROM Patient p WHERE p.name like :kw")
    List<Patient> findPatientByNameContains(@Param("kw") String kw);
}