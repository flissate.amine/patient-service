FROM openjdk:8-jdk-alpine
EXPOSE 8888
COPY ./target/patient-0.0.1-SNAPSHOT.jar /patient-0.0.1-SNAPSHOT.jar
ENTRYPOINT ["java","-jar","patient-0.0.1-SNAPSHOT.jar"]
